# Elm RPN Calculator

A simple Reverse Polish Notation calculator written in Elm.

## Development

I recommend using [elm-live](https://www.elm-live.com/) for a simple and convenient development setup.

```bash
npm i
npx elm-live src/Main.elm -- --output=elm.js
```

## Build

```bash
npm i
npm run build
```
